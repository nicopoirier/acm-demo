#!/bin/bash

export CLUSTER_NAME="bpce-demo-acm-1"
echo $CLUSTER_NAME
gcloud container clusters create $CLUSTER_NAME --region europe-west1 --num-nodes=1  --machine-type=e2-standard-4
gcloud container clusters get-credentials $CLUSTER_NAME --region=europe-west1